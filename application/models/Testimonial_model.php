<?php
/**
*
*/
class Testimonial_model extends CI_Model
{

  function get($batas=NULL,$offset=NULL,$cari=NULL)
  {
    if ($batas != NULL) {
      $this->db->limit($batas,$offset);
    }
    if ($cari != NULL) {
      $this->db->or_like($cari);
    }
    $this->db->order_by('date','asc');
    $this->db->from('tbl_testimonial');
    $query = $this->db->get();
    return $query->result();
  }
  function jumlah_row($search)
  {
    $this->db->or_like($search);
    $query = $this->db->get('tbl_testimonial');

    return $query->num_rows();
  }



  function get_by_id($kondisi)
  {
    $this->load->helper('url');
    $this->db->from('tbl_testimonial');
    $this->db->where($kondisi);
    $query = $this->db->get();
    return $query->row();

  }

  function get_by_title($kondisititle)
  {
    $this->db->from('tbl_testimonial');
    $this->db->where($kondisititle);
    $query = $this->db->get();
    return $query->result();
  }

  function get_by_category($kondisikategori)
  {
    $this->db->from('tbl_testimonial');
    $this->db->where($kondisikategori);
    $query = $this->db->get();
    return $query->result();
  }

  function insert($data)
  {
    $this->db->insert('tbl_testimonial',$data);
    return TRUE;
  }
  function delete($delete)
  {
    $this->db->where($delete);
    $this->db->delete('tbl_testimonial');
    return TRUE;
  }
  function update($data,$kondisi)
  {
    $this->db->update('tbl_testimonial',$data,$kondisi);
    return TRUE;
  }

}
