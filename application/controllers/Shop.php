<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('gallery_model');
    $this->load->helper('url');
    $this->load->library('upload');
    $this->load->library('pagination');


  }

  // fungsi untuk mengambil data
  public function index()
  {

    $this->data['data'] = $this->gallery_model->get();
    $this->data['title']   = 'Our Shop - Dotivity';
    $this->load->view('frontend/pages/our-shop/index',$this->data);

  }
}
