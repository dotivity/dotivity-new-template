<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('gallery_model');
    $this->load->library('pagination');
    $this->load->helper('slug_helper');
  }


  function time_ago($timestamp){

      //type cast, current time, difference in timestamps
      $timestamp      = (int) $timestamp;
      $current_time   = time();
      $diff           = $current_time - $timestamp;

      //intervals in seconds
      $intervals      = array (
          'year' => 31556926, 'month' => 2629744, 'week' => 604800, 'day' => 86400, 'hour' => 3600, 'minute'=> 60
      );

      //now we just find the difference
      if ($diff == 0)
      {
          return 'just now';
      }
      if ($diff < 60)
      {
          return $diff == 1 ? $diff . ' second ago' : $diff . ' seconds ago';
      }
      if ($diff >= 60 && $diff < $intervals['hour'])
      {
          $diff = floor($diff/$intervals['minute']);
          return $diff == 1 ? $diff . ' minute ago' : $diff . ' minutes ago';
      }
      if ($diff >= $intervals['hour'] && $diff < $intervals['day'])
      {
          $diff = floor($diff/$intervals['hour']);
          return $diff == 1 ? $diff . ' hour ago' : $diff . ' hours ago';
      }
      if ($diff >= $intervals['day'] && $diff < $intervals['week'])
      {
          $diff = floor($diff/$intervals['day']);
          return $diff == 1 ? $diff . ' day ago' : $diff . ' days ago';
      }
      if ($diff >= $intervals['week'] && $diff < $intervals['month'])
      {
          $diff = floor($diff/$intervals['week']);
          return $diff == 1 ? $diff . ' week ago' : $diff . ' weeks ago';
      }
      if ($diff >= $intervals['month'] && $diff < $intervals['year'])
      {
          $diff = floor($diff/$intervals['month']);
          return $diff == 1 ? $diff . ' month ago' : $diff . ' months ago';
      }
      if ($diff >= $intervals['year'])
      {
          $diff = floor($diff/$intervals['year']);
          return $diff == 1 ? $diff . ' year ago' : $diff . ' years ago';
      }
  }

  // fungsi untuk mengambil data
function Laser()
  {
    date_default_timezone_set("ASIA/JAKARTA");
    $cari = $this->input->get('cari');
    $page = $this->input->get('per_page');
    $kondisikategori = "pic_category='Laser'";
    $search = array('pic_title' => $cari );

    $batas =  10; // 9 data per page
    if(!$page):
      $offset = 0;
    else:
      $offset = $page;
    endif;

    $config['page_query_string'] = TRUE;
    $config['base_url'] 				 = base_url().'product_gallery/?cari='.$cari;
    $config['total_rows'] 			 = $this->gallery_model->jumlah_row($search);

    $config['per_page'] 				 = $batas;
    $config['uri_segment'] 			 = $page;

    $config['full_tag_open'] 		= '<ul class="pagination">';
    $config['full_tag_close'] 	= '<ul>';

    $config['first_link'] 			= 'first';
    $config['first_tag_open'] 	= '<li><a>';
    $config['first_tag_close'] 	= '</li>';

    $config['last_link'] 				= 'last';
    $config['last_tag_open']	 	= '<li>';
    $config['last_tag_close'] 	= '</a></li>';

    $config['next_link'] 				= '&raquo;';
    $config['next_tag_open'] 		= '<li>';
    $config['next_tag_close'] 	= '</li>';

    $config['prev_link'] 				= '&laquo;';
    $config['prev_tag_open'] 		= '<li>';
    $config['prev_tag_close'] 	= '</li>';

    $config['cur_tag_open'] 		= '<li class="active"><a>';
    $config['cur_tag_close'] 		= '</li>';

    $config['num_tag_open'] 		= '<li>';
    $config['num_tag_close'] 		= '</li>';

    $this->pagination->initialize($config);
    $data['pagination']	 = $this->pagination->create_links();
    $data['jumlah_page'] = $page;


    $this->data['data'] = $this->gallery_model->get_by_category($kondisikategori);
    $this->data['meta_desc']   = 'Laser Product - Dotivity';
    $this->data['title']   = 'Projects Laser Product - Dotivity';
    $this->load->view('frontend/english/projects/laser',$this->data);
  }

  // fungsi untuk mengambil data
 function Printing()
  {
    date_default_timezone_set("ASIA/JAKARTA");
    $cari = $this->input->get('cari');
    $page = $this->input->get('per_page');
    $kondisikategori = "pic_category='Printing'";
    $search = array('pic_title' => $cari );

    $batas =  10; // 9 data per page
    if(!$page):
      $offset = 0;
    else:
      $offset = $page;
    endif;

    $config['page_query_string'] = TRUE;
    $config['base_url'] 				 = base_url().'product_gallery/?cari='.$cari;
    $config['total_rows'] 			 = $this->gallery_model->jumlah_row($search);

    $config['per_page'] 				 = $batas;
    $config['uri_segment'] 			 = $page;

    $config['full_tag_open'] 		= '<ul class="pagination">';
    $config['full_tag_close'] 	= '<ul>';

    $config['first_link'] 			= 'first';
    $config['first_tag_open'] 	= '<li><a>';
    $config['first_tag_close'] 	= '</li>';

    $config['last_link'] 				= 'last';
    $config['last_tag_open']	 	= '<li>';
    $config['last_tag_close'] 	= '</a></li>';

    $config['next_link'] 				= '&raquo;';
    $config['next_tag_open'] 		= '<li>';
    $config['next_tag_close'] 	= '</li>';

    $config['prev_link'] 				= '&laquo;';
    $config['prev_tag_open'] 		= '<li>';
    $config['prev_tag_close'] 	= '</li>';

    $config['cur_tag_open'] 		= '<li class="active"><a>';
    $config['cur_tag_close'] 		= '</li>';

    $config['num_tag_open'] 		= '<li>';
    $config['num_tag_close'] 		= '</li>';

    $this->pagination->initialize($config);
    $data['pagination']	 = $this->pagination->create_links();
    $data['jumlah_page'] = $page;


    $this->data['data'] = $this->gallery_model->get_by_category($kondisikategori);
    $this->data['meta_desc']   = 'Laser Product - Dotivity';
    $this->data['title']   = 'Projects Home Printing - Dotivity';
    $this->load->view('frontend/english/projects/printing',$this->data);
  }

  // fungsi untuk mengambil data
  public function Sticker()
  {
    date_default_timezone_set("ASIA/JAKARTA");
    $cari = $this->input->get('cari');
    $page = $this->input->get('per_page');
    $kondisikategori = "pic_category='Sticker'";
    $search = array('pic_title' => $cari );

    $batas =  10; // 9 data per page
    if(!$page):
      $offset = 0;
    else:
      $offset = $page;
    endif;

    $config['page_query_string'] = TRUE;
    $config['base_url'] 				 = base_url().'product_gallery/?cari='.$cari;
    $config['total_rows'] 			 = $this->gallery_model->jumlah_row($search);

    $config['per_page'] 				 = $batas;
    $config['uri_segment'] 			 = $page;

    $config['full_tag_open'] 		= '<ul class="pagination">';
    $config['full_tag_close'] 	= '<ul>';

    $config['first_link'] 			= 'first';
    $config['first_tag_open'] 	= '<li><a>';
    $config['first_tag_close'] 	= '</li>';

    $config['last_link'] 				= 'last';
    $config['last_tag_open']	 	= '<li>';
    $config['last_tag_close'] 	= '</a></li>';

    $config['next_link'] 				= '&raquo;';
    $config['next_tag_open'] 		= '<li>';
    $config['next_tag_close'] 	= '</li>';

    $config['prev_link'] 				= '&laquo;';
    $config['prev_tag_open'] 		= '<li>';
    $config['prev_tag_close'] 	= '</li>';

    $config['cur_tag_open'] 		= '<li class="active"><a>';
    $config['cur_tag_close'] 		= '</li>';

    $config['num_tag_open'] 		= '<li>';
    $config['num_tag_close'] 		= '</li>';

    $this->pagination->initialize($config);
    $data['pagination']	 = $this->pagination->create_links();
    $data['jumlah_page'] = $page;


    $this->data['data'] = $this->gallery_model->get_by_category($kondisikategori);
    $this->data['meta_desc']   = 'Laser Product - Dotivity';
    $this->data['title']   = 'Projects Cutting Sticker - Dotivity';
    $this->load->view('frontend/english/projects/sticker',$this->data);
  }
}
