<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portfolio_website extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('website_model');
    $this->load->library('encrypt');
    $this->load->model('admin_model');
    $this->load->model('user_model');
    $this->load->model('info_model');
    $this->load->library('upload');
    $this->load->library('pagination');
    $this->load->helper('slug_helper');
    $this->user_model->check_role();

    if (!$this->session->userdata('cibb_user_id')) {
      redirect('gotobackend/login');
    }

  }


  function time_ago($timestamp){

    //type cast, current time, difference in timestamps
    $timestamp      = (int) $timestamp;
    $current_time   = time();
    $diff           = $current_time - $timestamp;

    //intervals in seconds
    $intervals      = array (
      'year' => 31556926, 'month' => 2629744, 'week' => 604800, 'day' => 86400, 'hour' => 3600, 'minute'=> 60
    );

    //now we just find the difference
    if ($diff == 0)
    {
      return 'just now';
    }
    if ($diff < 60)
    {
      return $diff == 1 ? $diff . ' second ago' : $diff . ' seconds ago';
    }
    if ($diff >= 60 && $diff < $intervals['hour'])
    {
      $diff = floor($diff/$intervals['minute']);
      return $diff == 1 ? $diff . ' minute ago' : $diff . ' minutes ago';
    }
    if ($diff >= $intervals['hour'] && $diff < $intervals['day'])
    {
      $diff = floor($diff/$intervals['hour']);
      return $diff == 1 ? $diff . ' hour ago' : $diff . ' hours ago';
    }
    if ($diff >= $intervals['day'] && $diff < $intervals['week'])
    {
      $diff = floor($diff/$intervals['day']);
      return $diff == 1 ? $diff . ' day ago' : $diff . ' days ago';
    }
    if ($diff >= $intervals['week'] && $diff < $intervals['month'])
    {
      $diff = floor($diff/$intervals['week']);
      return $diff == 1 ? $diff . ' week ago' : $diff . ' weeks ago';
    }
    if ($diff >= $intervals['month'] && $diff < $intervals['year'])
    {
      $diff = floor($diff/$intervals['month']);
      return $diff == 1 ? $diff . ' month ago' : $diff . ' months ago';
    }
    if ($diff >= $intervals['year'])
    {
      $diff = floor($diff/$intervals['year']);
      return $diff == 1 ? $diff . ' year ago' : $diff . ' years ago';
    }
  }

  // fungsi untuk mengambil data
  public function index()
  {
    date_default_timezone_set("ASIA/JAKARTA");
    $cari = $this->input->get('cari');
    $page = $this->input->get('per_page');

    $search = array('pic_title' => $cari );

    $batas =  100; // 9 data per page
    if(!$page):
      $offset = 0;
    else:
      $offset = $page;
    endif;

    $config['page_query_string'] = TRUE;
    $config['base_url'] 				 = base_url().'product_gallery/?cari='.$cari;
    $config['total_rows'] 			 = $this->website_model->jumlah_row($search);

    $config['per_page'] 				 = $batas;
    $config['uri_segment'] 			 = $page;

    $config['full_tag_open'] 		= '<ul class="pagination">';
    $config['full_tag_close'] 	= '<ul>';

    $config['first_link'] 			= 'first';
    $config['first_tag_open'] 	= '<li><a>';
    $config['first_tag_close'] 	= '</li>';

    $config['last_link'] 				= 'last';
    $config['last_tag_open']	 	= '<li>';
    $config['last_tag_close'] 	= '</a></li>';

    $config['next_link'] 				= '&raquo;';
    $config['next_tag_open'] 		= '<li>';
    $config['next_tag_close'] 	= '</li>';

    $config['prev_link'] 				= '&laquo;';
    $config['prev_tag_open'] 		= '<li>';
    $config['prev_tag_close'] 	= '</li>';

    $config['cur_tag_open'] 		= '<li class="active"><a>';
    $config['cur_tag_close'] 		= '</li>';

    $config['num_tag_open'] 		= '<li>';
    $config['num_tag_close'] 		= '</li>';

    $this->pagination->initialize($config);
    $data['pagination']	 = $this->pagination->create_links();
    $data['jumlah_page'] = $page;


    $data['data'] = $this->website_model->get($batas,$offset,$search);

    $tmp_success = $this->session->userdata('tmp_success');
    if ($tmp_success != NULL) {
      // user updated
      $this->session->unset_userdata('tmp_success');
      $this->data['tmp_success'] = 1;
    }

    $tmp_success_del = $this->session->userdata('tmp_success_del');
    if ($tmp_success_del != NULL) {
      // user deleted
      $this->session->unset_userdata('tmp_success_del');
      $this->data['tmp_success_del'] = 1;
    }
    $data['user']= $this->admin_model->get_by_id($this->session->userdata('cibb_user_id'));
    $this->data['title']   = 'Portfolio';
    $this->load->view('backend/portfolio/html', $this->data);
    $this->load->view('backend/portfolio/portfolio_website_list',$data);
  }

  // untuk menampilkan halaman tambah data
  public function add()
  {
    return $this->load->view('backend/upload_product');
  }

  public function insertdata()

  {

    $id   = $this->input->post('pic_id');
    $title   = $this->input->post('pic_title');
    $category = $this->input->post('pic_category');
    $d_main = $this->input->post('desc_main');
    $d_client = $this->input->post('desc_client');
    $d_project = $this->input->post('desc_project');
    $d_year = $this->input->post('desc_year');
    $slug = slug($this->input->post('pic_title',TRUE));


    $config['upload_path'] = './uploads/files/website/';
    $config['allowed_types'] = 'png|gif|jpg|jpeg|pjpeg|x-png';
    $config['max_size'] = '12048';  //12MB max
    $config['max_width'] = '4480'; // pixel
    $config['max_height'] = '4480'; // pixel
    $config['encrypt_name'] = TRUE;


    $pictures_thumbnail = array();
    $files_thumbnail = $_FILES;
    $cpt_thumbnail = count($_FILES['picture_thumbnail']['name']);
    for($i=0;$i<$cpt_thumbnail;$i++)
    {
      $_FILES['userfile']['name']= $files_thumbnail['picture_thumbnail']['name'][$i];
      $_FILES['userfile']['type']= $files_thumbnail['picture_thumbnail']['type'][$i];
      $_FILES['userfile']['tmp_name']= $files_thumbnail['picture_thumbnail']['tmp_name'][$i];
      $_FILES['userfile']['error']= $files_thumbnail['picture_thumbnail']['error'][$i];
      $_FILES['userfile']['size']= $files_thumbnail['picture_thumbnail']['size'][$i];
      $this->upload->initialize($config);
      $this->upload->do_upload();
      $pictures_thumbnail[] = $this->upload->data();
    }

    $pictures_thumbnail_main = array();
    $files_thumbnail_main = $_FILES;
    $cpt_thumbnail_main = count($_FILES['picture_thumbnail_main']['name']);
    for($i=0;$i<$cpt_thumbnail_main;$i++)
    {
      $_FILES['userfile']['name']= $files_thumbnail_main['picture_thumbnail_main']['name'][$i];
      $_FILES['userfile']['type']= $files_thumbnail_main['picture_thumbnail_main']['type'][$i];
      $_FILES['userfile']['tmp_name']= $files_thumbnail_main['picture_thumbnail_main']['tmp_name'][$i];
      $_FILES['userfile']['error']= $files_thumbnail_main['picture_thumbnail_main']['error'][$i];
      $_FILES['userfile']['size']= $files_thumbnail_main['picture_thumbnail']['size'][$i];
      $this->upload->initialize($config);
      $this->upload->do_upload();
      $pictures_thumbnail_main[] = $this->upload->data();
    }

    $pictures_header = array();
    $files_header = $_FILES;
    $cpt_header = count($_FILES['picture_header']['name']);
    for($i=0;$i<$cpt_header;$i++)
    {
      $_FILES['userfile']['name']= $files_header['picture_header']['name'][$i];
      $_FILES['userfile']['type']= $files_header['picture_header']['type'][$i];
      $_FILES['userfile']['tmp_name']= $files_header['picture_header']['tmp_name'][$i];
      $_FILES['userfile']['error']= $files_header['picture_header']['error'][$i];
      $_FILES['userfile']['size']= $files_header['picture_header']['size'][$i];
      $this->upload->initialize($config);
      $this->upload->do_upload();
      $pictures_header[] = $this->upload->data();
    }

    $pictures_screenshot = array();
    $files_screenshot = $_FILES;
    $cpt_screenshot = count($_FILES['picture_screenshot']['name']);
    for($i=0;$i<$cpt_screenshot;$i++)
    {
      $_FILES['userfile']['name']= $files_screenshot['picture_screenshot']['name'][$i];
      $_FILES['userfile']['type']= $files_screenshot['picture_screenshot']['type'][$i];
      $_FILES['userfile']['tmp_name']= $files_screenshot['picture_screenshot']['tmp_name'][$i];
      $_FILES['userfile']['error']= $files_screenshot['picture_screenshot']['error'][$i];
      $_FILES['userfile']['size']= $files_screenshot['picture_screenshot']['size'][$i];
      $this->upload->initialize($config);
      $this->upload->do_upload();
      $pictures_screenshot[] = $this->upload->data();
    }

    $pictures_screenshot_r = array();
    $files_screenshot_r = $_FILES;
    $cpt_screenshot_r = count($_FILES['picture_screenshot_r']['name']);
    for($i=0;$i<$cpt_screenshot_r;$i++)
    {
      $_FILES['userfile']['name']= $files_screenshot_r['picture_screenshot_r']['name'][$i];
      $_FILES['userfile']['type']= $files_screenshot_r['picture_screenshot_r']['type'][$i];
      $_FILES['userfile']['tmp_name']= $files_screenshot_r['picture_screenshot_r']['tmp_name'][$i];
      $_FILES['userfile']['error']= $files_screenshot_r['picture_screenshot_r']['error'][$i];
      $_FILES['userfile']['size']= $files_screenshot_r['picture_screenshot_r']['size'][$i];
      $this->upload->initialize($config);
      $this->upload->do_upload();
      $pictures_screenshot_r[] = $this->upload->data();
    }

    $data = array(
      'pic_title'                     => $title,
      'date'                          => date('Y-m-d H:i:s'),
      'pic_thumbnail'                 => $pictures_thumbnail[0]['file_name'],
      'pic_thumbnail_main'            => $pictures_thumbnail_main[0]['file_name'],
      'pic_header_1'                  => $pictures_header[0]['file_name'],
      'pic_header_2'                  => $pictures_header[1]['file_name'],
      'pic_header_3'                  => $pictures_header[2]['file_name'],
      'pic_screenshot_1'              => $pictures_screenshot[0]['file_name'],
      'pic_screenshot_2'              => $pictures_screenshot[1]['file_name'],
      'pic_screenshot_3'              => $pictures_screenshot[2]['file_name'],
      'pic_screenshot_4'              => $pictures_screenshot[3]['file_name'],
      'pic_screenshot_r_1'            => $pictures_screenshot_r[0]['file_name'],
      'pic_screenshot_r_2'            => $pictures_screenshot_r[1]['file_name'],
      'pic_screenshot_r_3'            => $pictures_screenshot_r[2]['file_name'],
      'pic_screenshot_r_4'            => $pictures_screenshot_r[3]['file_name'],
      'pic_category'                  => $category,
      'desc_main'                     => $d_main,
      'desc_client'                   => $d_client,
      'desc_project'                  => $d_project,
      'desc_year'                     => $d_year,
      'pic_slug'                      => $slug,
    );

    $this->website_model->insert($data,$pictures_thumbnail,$pictures_thumbnail_main,$pictures_header,$pictures_screenshot,$pictures_screenshot_r);
    redirect('backend/portfolio_website',$data);

  }

  // delete
  public function deletedata($id)
  {
    $path = './uploads/files/website';
    @unlink($path);

    $id = array('pic_id' => $id );
    $this->website_model->delete($id);
    return redirect('backend/portfolio_website');
  }


  // edit
  public function edit($id)
  {
    $kondisi = array('pic_id' => $id );

    $data['data'] = $this->website_model->get_by_id($kondisi);
    return $this->load->view('backend/edit_product',$data);
  }

  // update
  public function updatedata()
  {
    date_default_timezone_set("ASIA/JAKARTA");
    $id   = $this->input->post('pic_id');
    $title   = $this->input->post('pic_title');
    $category = $this->input->post('pic_category');
    $desc = $this->input->post('pic_desc');
    $slug = slug($this->input->post('pic_title',TRUE));
    $path = './uploads/files/';
    $kondisi = array('pic_id' => $id );


    $picture = array();
    $files = $_FILES;
    $cpt = count($_FILES['userfile']['name']);
    for($i=0; $i<$cpt; $i++)
    {
      $_FILES['userfile']['name']= $files['userfile']['name'][$i];
      $_FILES['userfile']['type']= $files['userfile']['type'][$i];
      $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
      $_FILES['userfile']['error']= $files['userfile']['error'][$i];
      $_FILES['userfile']['size']= $files['userfile']['size'][$i];

      $config['upload_path'] = './uploads/files/';
      $config['allowed_types'] = 'png|gif|jpg|jpeg|pjpeg|x-png';
      $config['max_size'] = '2048';  //2MB max
      $config['max_width'] = '4480'; // pixel
      $config['max_height'] = '4480'; // pixel
      $config['encrypt_name'] = TRUE;

      $this->upload->initialize($config);
      if (!empty($_FILES['userfile']['name'])) {
        if ( $this->upload->do_upload() ) {
          $picture[] = $this->upload->data();

          $data = array(
            'pic_title'     => $title,
            'date'          => date('Y-m-d H:i:s'),
            'pic_file'      => $picture[0]['file_name'],
            'pic_file1'     => $picture[1]['file_name'],
            'pic_file2'     => $picture[2]['file_name'],
            'pic_file3'     => $picture[3]['file_name'],
            'pic_file4'     => $picture[4]['file_name'],
            'pic_category'  => $category,
            'pic_desc'      => $desc,
            'pic_slug'      => $slug,
          );
          // hapus foto pada direktori
        }
      }
    }
    // hapus foto pada direktori
    @unlink($path.$this->input->post('filelama'));
    $this->website_model->update($data,$kondisi);
    return $this->load->view('backend/success_upload');
  }
} // end class
