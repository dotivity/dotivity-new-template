<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('gallery_model');
    $this->load->helper('url');
    $this->load->library('upload');
    $this->load->library('pagination');


  }

  // fungsi untuk mengambil data
  public function index()
  {

    $this->data['data'] = $this->gallery_model->get();
    $this->data['title']   = 'Industri Kreatif Online - Dotivity';
    $this->load->view('frontend/pages/home/index',$this->data);

  }
}
