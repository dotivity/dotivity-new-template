<div id="main" class="content">

  <!-- Page Settings -->
  <div class="page-settings" data-layout="dark" data-header-style="light" data-menu-layout="light"></div>
  <!--/ Page settings -->

  <!-- Page Content -->
  <div id="page-content" class="page-content">

    <!-- Section -->
    <div class="section">

      <!-- Wrapper (Small) -->
      <div class="wrapper-small">

        <!-- Column -->
        <div class="c-col-12">

          <!-- Text Wrapper -->
          <div class="text-wrapper">
            <div class="caption has-animation skew-up">Tentang Kami</div>
            <h1 class="big-title has-animation skew-up">
              Halo! <br> Kami Dotivity.
            </h1>
          </div>
          <!--/ Text Wrapper -->

        </div>
        <!--/ Column -->

        <div class="c-col-4 hide-mobile"></div>

        <!-- Column -->
        <div class="c-col-8">

          <!-- Text Wrapper -->
          <div class="text-wrapper">
            <p data-delay="0.4" class="big-p has-animation lines-up">Sebuah industri kreatif yang bergerak di bidang media promosi.<br>

            </p>
          </div>
          <!--/ Text Wrapper -->

        </div>
        <!--/ Column -->

      </div>
      <!--/ Wrapper -->


    </div>
    <!--/ Section -->

    <!-- Section -->
    <div class="section">

      <!-- Wrapper -->
      <div class="wrapper">

        <!-- Column -->
        <div class="c-col-12 has-animation fade-in-up">

          <!-- Embed Video -->
          <div class="pe-embed-video">
            <div class="pe-video" data-plyr-provider="youtube" data-plyr-embed-id="FjoZWxgXwSA"></div>
          </div>
          <!--/ Embed Video -->

        </div>
        <!--/ Column -->

      </div>
      <!--/ Wrapper -->

    </div>
    <!--/ Section -->

    <!-- Section -->
    <div class="section">

      <!-- Wrapper -->
      <div class="wrapper-small">

        <!-- Column -->
        <div class="c-col-6">

          <!-- Text Wrapper -->
          <div class="text-wrapper">
            <div data-delay="0.5" class="caption has-animation skew-up">LAYANAN KAMI</div>
            <h1 class="big-title has-animation skew-up">Apa yang kami lakukan?</h1>
          </div>
          <!-- Text Wrapper -->

        </div>
        <!--/ Column -->

        <!-- Column -->
        <div class="c-col-6">

          <span class="pe-empty-space" style="height: 300px"></span>

          <!-- Text Wrapper -->
          <div class="text-wrapper">
            <h4 class="thin has-animation lines-up">
            Kami secara khusus fokus pada produk stiker, produk laser (Cutting / Marking / Enraving), percetakan rumahan, dan produk periklanan lainnya.
            </h4>
          </div>
          <!--/ Text Wrapper -->



        </div>
        <!--/ Column -->

      </div>
      <!--/ Wrapper -->

    </div>
    <!--/ Section -->

    <!-- Section -->
    <div class="section" data-background="#0b0b0b">

      <!-- Wrapper -->
      <div class="wrapper-small">

        <div class="c-col-6 hide-mobile"></div>

        <!-- Column -->
        <div class="c-col-6">

          <!-- Text Wrapper -->
          <div class="text-wrapper">
            <div style="color: #ededed" class="caption has-animation skew-up">TARGET KAMI</div>
            <h1 style="color: #ededed" class="big-title has-animation skew-up">
              Visi & Misi Dotivity
            </h1>

          </div>
          <!--/ Text Wrapper -->

        </div>
        <!--/ Column -->

        <!-- Column -->
        <div class="c-col-8">

          <!-- Text Wrapper -->
          <div class="text-wrapper">
            <div style="color: #ededed" class="caption has-animation skew-up">Visi</div>
            <h3 style="color: #ededed" class="thin has-animation lines-up">
            Mempermudah pelanggan dari seluruh negeri untuk mewujudkan kreativitas mereka dengan satu sentuhan melalui Dotivity.</h3>

            </div>
            <!--/ Text Wrapper -->

          </div>
          <!--/ Column -->

        </div>
        <!--/ Wrapper -->

        <!-- Wrapper -->
        <div class="wrapper-full">

          <!-- Column -->
          <div class="c-col-12 no-gap">

            <!-- Image Wrapper -->
            <div class="image-wrapper send-back">
              <img src="<?php echo base_url('assets_frontend/light/images/about/doors.jpg')?>" alt="Goals Image">
            </div>
            <!--/ Image Wrapper -->

          </div>
          <!--/ Column -->

        </div>
        <!--/ Wrapper -->

        <!-- Wrapper -->
        <div class="wrapper-small">

          <!-- Column -->
          <div class="c-col-8">

            <!-- Text Wrapper -->
            <div class="text-wrapper">
              <div style="color: #ededed" class="caption has-animation skew-up">Misi</div>
              <h4 style="color: #ededed" class="thin has-animation lines-up">
- Memastikan pelanggan dapat dengan mudah mengakses layanan Dotivity dari mana saja.
<br><br>
- Menyediakan beragam opsi produk dan layanan yang sesuai dengan kebutuhan kreatif pelanggan.
<br><br>
- Merespons dengan cepat setiap pertanyaan dan masukan pelanggan.
<br><br>
- Terus mengembangkan teknologi baru untuk pengalaman pelanggan yang lebih baik.
<br><br>
- Menjadi mitra yang dapat diandalkan bagi pelanggan dalam mengekspresikan kreativitas mereka.             

</h4>
            </div>
            <!--/ Text Wrapper -->

          </div>
          <!--/ Column -->

        </div>
        <!--/ Wrapper -->

      </div>
      <!-- Section -->

      <!--/ Section -->
      <div class="section">

        <!-- Wrapper -->
        <div class="wrapper-small">

          <!-- Column -->
          <div class="c-col-6">

            <!-- Text Wrapper -->
            <div class="text-wrapper">
              <div class="caption">LANGKAH KAMI</div>
              <h1 class="big-title">UNTUK PELANGGAN</h1>
            </div>
            <!--/ Text Wrapper -->

          </div>
          <!--/ Column -->

          <!-- Column -->
          <div class="c-col-6">

            <span class="pe-empty-space" style="height: 300px"></span>

            <!-- Text Wrapper -->
            <div class="text-wrapper">
              <p class="big-p">Kami adalah penggemar kesetiaan dan kejujuran dalam setiap langkah kami. Kami bukan hanya mitra dalam proyek Anda, tetapi juga teman yang memahami dan mendukung visi Anda.
<br><br>
Kami percaya bahwa hubungan yang kuat dengan klien adalah kunci kesuksesan. Komunikasi terbuka dan hubungan yang dekat membuat proses kolaborasi menjadi menyenangkan dan produktif. Mari bersama-sama menjadikan impian Anda menjadi kenyataan!
<br><br>

1. <b>Hubungi Kami:</b> Kirim pesan pertama melalui WhatsApp atau email untuk mengungkapkan minat Anda dalam memesan produk kustom. Berikan detail singkat tentang apa yang Anda inginkan.
<br><br>
2. <b>Konsultasi Desain:</b> Tim kami akan merespons pesan Anda dan menjadwalkan konsultasi desain, jika diperlukan. Anda dapat berbagi ide, preferensi, dan spesifikasi yang diinginkan.
<br><br>
3. <b>Kirim Gambar/Logo:</b> Jika Anda memiliki gambar atau logo yang ingin digunakan, Anda dapat mengirimkannya melalui WhatsApp atau email. Pastikan gambar/logo tersebut memiliki kualitas yang baik.
<br><br>
4. <b>Konfirmasi Harga:</b> Setelah menerima gambar/logo dan detail lainnya, tim kami akan memberikan perkiraan harga untuk produk kustom yang diminta. Pastikan untuk konfirmasi harga dan detail pesanan sebelum melanjutkan.
<br><br>
5. <b>Pembayaran:</b> Setelah semua detail disepakati, tim kami akan memberikan informasi pembayaran melalui WhatsApp atau email. Pembayaran dapat dilakukan melalui transfer bank atau metode pembayaran lain yang disepakati.
<br><br>
6. <b>Pengerjaan Produk:</b> Setelah pembayaran dikonfirmasi, tim kami akan memulai proses pembuatan produk kustom sesuai dengan spesifikasi yang telah disepakati.
<br><br>
7. <b>Pengiriman:</b> Setelah produk selesai dibuat, kami akan menghubungi Anda untuk konfirmasi pengiriman. Produk akan dikirimkan ke alamat yang telah Anda berikan.
<br><br>
8. <b>Konfirmasi Penerimaan:</b> Setelah menerima produk, pastikan untuk memberikan konfirmasi penerimaan melalui WhatsApp atau email dan berikan umpan balik tentang pengalaman Anda.

Dengan mengikuti langkah-langkah ini, Anda dapat dengan mudah memesan produk kustom melalui WhatsApp atau email dengan tim kami.
              </p>
            </div>
            <!--/ Text Wrapper -->

          </div>
          <!--/ Column -->

        </div>
        <!--/ Wrapper -->

        <!-- Wrapper -->


      </div>
      <!-- Section -->

      <!--/ Section -->
      <div class="section">

        <!-- Wrapper -->
        <div class="wrapper-small">

          <!-- Column -->
          <div class="c-col-12 align-center">
            <div class="caption has-animation skew-up">WHAT WE DID BEFORE</div>
            <h1 data-delay="0.2" class="big-title has-animation skew-up"><a href="works.html" class="underline">Projects</a></h1>
          </div>
          <!--/ Column -->

        </div>
        <!--/ Wrapper -->

      </div>
      <!--/ Section -->

    </div>
    <!--/ Page Content -->
