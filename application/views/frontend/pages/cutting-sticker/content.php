<!-- Site Content -->
<div id="main" class="content">

  <!-- Page Settings -->
  <div class="page-settings" data-layout="dark" data-header-style="light" data-menu-layout="light"></div>
  <!--/ Page Settings -->

  <!-- Single Project -->
  <div class="single-project">


    <!-- Project Header -->
    <div class="project-header light">

      <!-- Project Image -->
      <div class="project-image project-image-full ">
        <img src="<?php echo base_url('assets_frontend/light/images/cutting-sticker/header.jpg')?>" alt="Project Header Image">
      </div>
      <!--/ Project Image -->

      <div class="project-top">

        <!-- Project Category -->
        <div data-delay=".2" class="project-work has-animation skew-down">
          Produk Kami
        </div>
        <!-- Project Category -->

        <!-- Project Title -->
        <div class="project-title has-animation skew-up">
          Custom Sticker
        </div>
        <!-- Project Title -->

      </div>


      <!-- ENGLISH !!!
      <div data-delay="1" class="project-summary has-animation lines-up">
        At Dotivity, you're controlling everything. Effectively make totally adaptable, custom stickers in any shape, size, and amount. Basically send your picture or logo to our <a href="https://wa.me/6281388813751">Whatsapp</a> or <a href="mailto:hello@dotivity.id">Email.</a>
      </div>-->

      <div data-delay="1" class="project-summary has-animation lines-up">
      Di Dotivity, semua di bawah kendali Anda! Buat stiker khusus yang sepenuhnya disesuaikan dalam berbagai ukuran, bentuk, dan jumlah. Kirim gambar atau logo Anda melalui <a href="mailto:hello@dotivity.id">Email</a> atau <a href="https://wa.me/6281388813751">Whatsapp</a> kami.
      </div>
      <!-- Project Summary -->

    </div>
    <!--/ Project Header -->

    <!-- Project Content -->
    <div class="project-content">

      <!-- Section -->
      <div class="section">

        <!-- Wrapper (Small) -->
        <div class="wrapper-small">

          <!-- Column -->
          <div class="c-col-12">

            <!-- Text wrapper -->
            <div class="text-wrapper">

              <div class="caption has-animation skew-up">STICKER SEBAGAI MEDIA PROMOSI</div>

              <h3 class="has-animation lines-fade-up">
              Untuk meningkatkan angka penjualan sebuah produk, pastinya kita sebagai pelaku bisnis melakukan promosi. Tindakan marketing ini sangat berpengaruh untuk meningkatkan penjualan dan ke-eksistensian sebuah produk atau jasa.
              </h3>
              <p data-delay="0.2" class="big-p has-animation lines-fade-up">
              Stiker menjadi salah satu alat promosi yang berbebentuk label. Memiliki perekat pada bagian belakangnya sehingga dapat ditempelkan pada kemasan. Jenis stiker pun berbeda-beda, bisa dibedakan berdasarkan jenis kertas, bentuk, dan cutting-annya.
              <br>
              <br>
              Meskipun cara promosi kini sudah banyak yang menggunakan secara online, tidak menutup kemungkinan pemasaran dengan cara offline tetap bisa efektif. Salah satu media promosi offline yang masih dikatakan efektif adalah stiker. Dengan menggunakan stiker, bisa mempercantik kemasan produk dan jadi salah satu cara untuk meningkatkan penjualan.
              </p>

            </div>
            <!--/ Text wrapper -->

          </div>
          <!--/ Column -->

        </div>
        <!--/ Wrapper (Small) -->

      </div>
      <!--/ Section -->

      <!-- Section -->
      <div class="section">

        <!-- Wrapper (Fullwidth) -->
        <div class="wrapper-full">

          <!-- Column -->
          <div class="c-col-12 no-gap">

            <!-- Image Wrapper -->
            <div class="image-wrapper">
              <img src="<?php echo base_url('assets_frontend/light/images/cutting-sticker/section-1.jpg')?>" alt="Single Image">
            </div>
            <!--/ Image Wrapper -->

          </div>
          <!--/ Column -->

        </div>
        <!-- Wrapper (Fullwidth) -->

      </div>
      <!--/ Section -->


      <!-- Section -->
      <div class="section">

        <!-- Wrapper -->
        <div class="wrapper-small">

          <div class="c-col-4 hide-mobile"></div>

          <!-- Column -->
          <div class="c-col-8">

            <!-- Text Wrapper -->
            <div class="text-wrapper">

              <div class="caption has-animation skew-up">KEUNGGULAN STICKER</div>
              <h3 class="thin has-animation lines-up">
              Banyak pelaku usaha tidak mengetahui fungsi dari membuat sticker dengan desain sendiri. Sticker bagi mereka tidak terlalu penting. Padahal sticker dapat menjadi media untuk menginformasikan produknya. Mereka tidak menyadari kalau sticker dengan ukurannya yang kecil dapat menjadi media iklan yang ampuh. Dengan penggunaan sticker dapat meningkatkan brand-awarness untuk produk mereka.</h3>

              </div>
              <!--/ Text Wrapper -->

            </div>
            <!--/ Column -->

          </div>
          <!--/ Wrapper -->

        </div>
        <!--/ Section -->



        <!-- Section -->
        <div class="section">

          <!-- Wrapper (Small) -->
          <div class="wrapper-small">

            <!-- Column -->
            <div class="c-col-6">

              <!-- Text Wrapper -->
              <div class="text-wrapper">

                <h1 class="big-title has-animation skew-up">
                  Jenis Sticker
                </h1>

              </div>
              <!--/ Text Wrapper -->

            </div>
            <!--/ Column -->

            <!-- Column -->
            <div class="c-col-6 hide-mobile"></div>
            <!--/ Column -->

            <!-- Column -->
            <div class="c-col-6">


              <span class="pe-empty-space" style="height: 300px"></span>

              <!-- Text Wrapper -->
              <div class="text-wrapper">
                <h4 class="thin has-animation lines-up">
                Selanjutnya kita akan mulai membahas jenis stiker apa saja yang tersedia, dan bagaimana aplikasi yang pas untuk jenis stiker tertentu dapat digunakan secara ideal.
                  <br>
                  Diantaranya adalah :
                </h4>
              </div>
              <!--/ Text Wrapper -->

              <!-- Accordion -->
              <div class="c-accordion">
                <div class="accordion-list">
                  <ul>
                    <li class="accordion-title">Sticker Oracal
                      <p class="accordion-content ac-active">
                      Stiker oracal merupakan salah satu jenis stiker outdoor yang umum untuk kebutuhan outdoor yang membutuhkan daya tahan ekstra dibandingkan dengan stiker vinyl.
                         <br>
                         <br>
                         Penggunaannya yang paling sering adalah sebagai cutting sticker pada motor, mobil, helm, dan lainnya yang biasanya lebih mudah dilepas dan tidak terlalu meninggalkan bekas di permukaan.                    </p>
                    </li>
                    <li class="accordion-title">Sticker Vynil
                      <p class="accordion-content">
                      Stiker vinyl adalah stiker yang terbuat dari bahan vinyl yang sangat tahan terhadap air dan memiliki kelenturan yang baik dibandingkan dengan kertas HVS, menjadikannya sebagai stiker outdoor.
                        <br><br>
                        Di antara keunggulan stiker vinyl ini adalah:
                        <br>
                        <br>
                        - Tidak luntur,<br>
                         - Mudah dirawat dan dibersihkan,<br>
                         - Cocok untuk penggunaan di luar ruangan<br>
                         - Penggunaan jangka panjang<br>
                        <br>
                        Penggunaan stiker ini sangat bervariasi, namun umum untuk membuat stiker pada jendela mobil, spanduk, poster, dan berbagai kebutuhan lainnya.
                      </li>
                      <li class="accordion-title">Sticker Chromo
                        <p class="accordion-content">
                        Stiker kromo atau chromo adalah stiker yang terbuat dari bahan kertas yang dapat menempel begitu kuat pada suatu permukaan sehingga sulit terkelupas dan meninggalkan bekas.
                          <br>
                          <br>
                          Biaya produksi yang murah menjadi salah satu alasan mengapa stiker ini cukup banyak digunakan, terutama untuk kebutuhan dalam ruangan, dan biasa digunakan untuk mencetak logo bisnis atau merek yang mereka bagikan kepada pelanggan atau calon pelanggan.</li>
                        <li class="accordion-title">Sticker Reflektif
                          <p class="accordion-content">
                          Stiker scotchlite adalah stiker reflektif yang dapat memantulkan cahaya warna permukaannya, terutama jika terkena sinar matahari atau cahaya, sehingga dapat terlihat jelas pada malam hari.
                            <br>
                            <br>
                            Penggunaan stiker ini bisa kita temukan pada kendaraan dinas seperti mobil polisi, ambulan, petasan, dan lain sebagainya, atau pada seragam teknisi jalan raya, tambang, rambu-rambu jalan, dan lainnya.</li>
                          <li class="accordion-title">Sticker Transparan
                            <p class="accordion-content">
                            Stiker transparan, seperti namanya, menggunakan bahan yang tidak terlihat.
                              <br>
                              <br>
                              Stiker ini juga cukup umum di kaca jendela mobil atau di permukaan kaca lainnya seperti pajangan toko dan lain-lain</li>
                          </ul>
                        </div>
                      </div>
                      <!--/ Accordion -->

                    </div>
                    <!--/ Column -->

                  </div>
                  <!--/ Wrapper (Small) -->

                </div>
                <!--/ Section -->

                <!-- Section -->
                <div class="section">

                  <!-- Wrapper -->
                  <div class="wrapper">

                    <!-- Column -->
                    <div class="c-col-6">

                      <!-- Image Wrapper -->
                      <div class="image-wrapper image-lightbox">
                        <img src="<?php echo base_url('assets_frontend/light/images/cutting-sticker/chromo.jpg')?>" alt="Single Image">
                      </div>
                      <!--/ Image Wrapper -->

                    </div>
                    <!--/ Column -->

                    <!-- Column -->
                    <div class="c-col-6">

                      <!-- Image Wrapper -->
                      <div class="image-wrapper image-lightbox">
                        <img src="<?php echo base_url('assets_frontend/light/images/cutting-sticker/vynil.jpg')?>" alt="Single Image">
                      </div>
                      <!--/ Image Wrapper -->

                    </div>
                    <!--/ Column -->

                    <!-- Column -->
                    <div class="c-col-12">

                      <!-- Image Wrapper -->
                      <div class="image-wrapper">
                        <img src="<?php echo base_url('assets_frontend/light/images/cutting-sticker/oracal.jpg')?>" alt="Single Image">
                      </div>
                      <!--/ Image Wrapper -->

                    </div>
                    <!--/ Column -->

                    <!-- Column -->
                    <div class="c-col-6">

                      <!-- Image Wrapper -->
                      <div class="image-wrapper image-lightbox">
                        <img src="<?php echo base_url('assets_frontend/light/images/cutting-sticker/reflective.jpg')?>" alt="Single Image">
                      </div>
                      <!--/ Image Wrapper -->

                    </div>
                    <!--/ Column -->

                    <!-- Column -->
                    <div class="c-col-6">

                      <!-- Image Wrapper -->
                      <div class="image-wrapper image-lightbox">
                        <img src="<?php echo base_url('assets_frontend/light/images/cutting-sticker/transparent.jpg')?>" alt="Single Image">
                      </div>
                      <!--/ Image Wrapper -->

                    </div>
                    <!--/ Column -->

                  </div>
                  <!-- Wrapper -->

                </div>
                <!--/ Section -->




                <!-- Section -->
                <div class="section">

                  <!-- Wrapper -->
                  <div class="wrapper">

                    <!-- Column -->
                    <div class="c-col-12">

                      <!-- Image Wrapper -->
                      <div class="image-wrapper has-animation slide-top image-lightbox">
                        <img src="<?php echo base_url('assets_frontend/light/images/cutting-sticker/transparent.jpg')?>" alt="Single Image">
                      </div>
                      <!--/ Image Wrapper -->

                    </div>
                    <!--/ Column -->

                  </div>
                  <!-- Wrapper -->

                  <!-- Wrapper -->
                  <div class="wrapper">

                    <!-- Column -->
                    <div class="c-col-12">

                      <!-- Image Wrapper -->
                      <div class="image-wrapper has-animation slide-top image-lightbox">
                        <img src="<?php echo base_url('assets_frontend/light/images/cutting-sticker/transparent.jpg')?>" alt="Single Image">
                      </div>
                      <!--/ Image Wrapper -->

                    </div>
                    <!--/ Column -->

                  </div>
                  <!-- Wrapper -->

                </div>
                <!--/ Section -->

                <!-- Section -->
                <div class="section">

                  <!-- Wrapper (Small) -->
                  <div class="wrapper-small">

                    <!-- Column -->
                    <div class="c-col-12">

                      <!-- Text Wrapper -->
                      <div class="text-wrapper align-center">

                        <div class="caption">CLIENT SAYS:</div>

                        <h1 class="thin has-animation lines-up">

                          "Draw a line in the sand one-sheet we need a recap by eod, cob or whatever comes first. Productize open door policy horsehead offer, make corporate."

                        </h1>

                      </div>
                      <!--/ Text Wrapper -->

                    </div>
                    <!--/ Column -->

                  </div>
                  <!--/ Wrapper (Small) -->

                </div>
                <!--/ Section -->


              </div>
              <!-- Project Content -->

              <!-- Next Project Setion -->
              <div class="projects-nav">


                <div class="next-project">
                  <!-- Next Project URL -->
                  <a href="<?php echo site_url('laser')?>">
                    <div class="next-project-wrapper">

                      <span class="next-project-span has-animation skew-up">Layanan Lainnya </span>

                      <!-- Next Project Title -->
                      <h1 class="next-project-title has-animation skew-up">Jasa Laser Cutting</h1>
                      <!--/ Next Project Title -->
                    </div>

                    <!-- Next Project Image -->
                    <div class="next-project-image-wrapper">
                      <img src="<?php echo base_url('assets_frontend/light/images/laser-services/header1.jpg')?>" alt="Next Project Image">
                    </div>
                    <!--/ Next Project Image -->

                  </a>
                  <!--/ Next Project URL -->
                </div>


              </div>
              <!--/ Next Project Setion -->

            </div>
            <!--/ Single Project -->
