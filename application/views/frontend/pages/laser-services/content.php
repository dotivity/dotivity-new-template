<!-- Site Content -->
<div id="main" class="content">

  <!-- Page Settings -->
  <div class="page-settings" data-layout="dark" data-header-style="light" data-menu-layout="light"></div>
  <!--/ Page Settings -->

  <!-- Single Project -->
  <div class="single-project">


    <!-- Project Header -->
    <div class="project-header light">

      <!-- Project Image -->
      <div class="project-image">
        <img src="<?php echo base_url('assets_frontend/light/images/laser-services/header1.jpg')?>" alt="Project Header Image">
      </div>
      <!--/ Project Image -->


      <div class="project-top">

        <!-- Project Category -->
        <div data-delay=".2" class="project-work has-animation skew-down">
          Jasa Laser Cutting
        </div>
        <!-- Project Category -->

        <!-- Project Title -->
        <div class="project-title has-animation skew-up">
        Eksplorasi Tanpa Batas dengan Laser Cutting Akrilik!
        </div>
        <!-- Project Title -->

      </div>

      <!-- Project Summary -->

      <div data-delay="1" class="project-summary has-animation lines-up">
      Selamat datang di tempat yang memadukan teknologi mutakhir dengan seni yang memukau: laser cutting akrilik! Di sini, imajinasi Kamu adalah satu-satunya batasan. Dari desain yang unik hingga proyek kreatif yang menginspirasi, kami siap membantu Kamu mewujudkan setiap ide Kamu dengan presisi yang luar biasa.
      </div>
      <!-- Project Summary -->

    </div>
    <!--/ Project Header -->


    <!-- Project Content -->
    <div class="project-content">



      <!-- Section -->
      <div class="section">

        <!-- Wrapper (Small) -->
        <div class="wrapper-small">

          <!-- Column -->
          <div class="c-col-8">

            <!-- Text Wrapper -->
            <div class="text-wrapper">

              <h1 class="big-title has-animation skew-up">
              Terangi Ide-mu dengan Keajaiban Laser!
              </h1>

            </div>
            <!--/ Text Wrapper -->

          </div>
          <!--/ Column -->

          <!-- Column -->
          <div class="c-col-6 hide-mobile"></div>
          <!--/ Column -->

          <!-- Column -->
          <div class="c-col-6">

            <!-- Text Wrapper -->
            <div class="text-wrapper">

              <p class="big-p has-animation lines-up">
              Di sini, kami membuka pintu menuju dunia laser cutting akrilik yang mengagumkan. Dari pesona desain yang dipotong dengan presisi hingga sentuhan magis yang mengubah ide menjadi kenyataan, mari kita jelajahi potensi tak terbatas dari karya seni ini bersama-sama.
<br><br>
Kami bukan hanya menawarkan layanan laser cutting biasa. Kami adalah arsitek yang mengubah imajinasi menjadi bentuk nyata. Dengan teknologi canggih dan keterampilan seni yang luar biasa, setiap proyek kami adalah perpaduan sempurna antara ketepatan dan keindahan.
<br><br>
Tidak ada lagi batasan untuk kreativitas Kamu. Bersama, kita dapat menciptakan karya-karya yang memukau dan menginspirasi. Bergabunglah dengan kami dalam petualangan tak terlupakan ini, di mana setiap potongan akrilik menjadi langkah menuju keajaiban yang lebih besar!
              </div>
              <!--/ Text Wrapper -->

            </div>
            <!--/ Column -->

          </div>
          <!--/ Wrapper (Small) -->

        </div>
        <!--/ Section -->

        <!-- Section -->
        <div class="section">

          <!-- Wrapper -->
          <div class="wrapper">

            <!-- Column -->
            <div class="c-col-12">

              <!-- Image Wrapper -->
              <div class="image-wrapper">
                <img src="<?php echo base_url('assets_frontend/light/images/laser-services/1.jpg')?>" alt="Single Image">
              </div>
              <!--/ Image Wrapper -->

            </div>
            <!--/ Column -->

            <div class="c-col-3 hide-mobile"></div>
          </div>
          <!-- Wrapper -->

          <!-- Wrapper -->
          <div class="wrapper">

            <!-- Column -->
            <div class="c-col-12">

              <!-- Image Wrapper -->
              <div class="image-wrapper">
                <img src="<?php echo base_url('assets_frontend/light/images/laser-services/3.jpg')?>" alt="Single Image">
              </div>
              <!--/ Image Wrapper -->

            </div>
            <!--/ Column -->

            <div class="c-col-3 hide-mobile"></div>
          </div>
          <!-- Wrapper -->

        </div>
        <!--/ Section -->


        <!-- Section -->
        <div class="section">

          <!-- Wrapper -->
          <div class="wrapper-small">

            <!-- Column -->
            <div class="c-col-8">

              <!-- Text Wrapper -->
              <div class="text-wrapper">

                <div class="caption has-animation skew-up">Wujudkan Kreatifitasmu!</div>
                <h1 class="thin has-animation lines-up">
                  <b>Mari Berkolaborasi dalam Kreativitas:<b>
                </h1><br><br>
                <p class="big-p has-animation lines-up">
Ketika kreativitas bertemu cutting laser, hasilnya adalah inovasi yang tak terhentikan. Kami mengundang Kamu untuk menjadi bagian dari revolusi ini, untuk berkolaborasi dalam menciptakan solusi yang tidak terduga, mengubah proyek menjadi karya seni yang tak terlupakan.
                </p>
                </div>
                <!--/ Text Wrapper -->

              </div>
              <!--/ Column -->

            </div>
            <!--/ Wrapper -->

          </div>
          <!--/ Section -->

          <!-- Section -->
          <div class="section">

            <!-- Wrapper -->
            <div class="wrapper-full">

              <div class="c-col-3 hide-mobile"></div>

              <!-- Column -->
              <div class="c-col-9 no-gap">

                <!-- Image Wrapper -->
                <div class="image-wrapper has-animation slide-right">
                  <img src="<?php echo base_url('assets_frontend/light/images/laser-services/2.jpg')?>" alt="Single Image">
                </div>
                <!--/ Image Wrapper -->

              </div>
              <!--/ Column -->

              <div class="c-col-3 hide-mobile"></div>
            </div>
            <!-- Wrapper -->

            <!-- Wrapper -->
            <div class="wrapper-full">

              <!-- Column -->
              <div class="c-col-9 no-gap">

                <!-- Image Wrapper -->
                <div class="image-wrapper has-animation slide-left">
                  <img src="<?php echo base_url('assets_frontend/light/images/laser-services/4.jpg')?>" alt="Single Image">
                </div>
                <!--/ Image Wrapper -->

              </div>
              <!--/ Column -->

              <div class="c-col-3 hide-mobile"></div>
            </div>
            <!-- Wrapper -->


            <!-- Wrapper -->
            <div class="wrapper">

              <!-- Column -->
              <div class="c-col-12">

                <!-- Image Wrapper -->
                <div class="image-wrapper image-lightbox has-animation slide-top">
                  <img src="../light/images/projects/sand-of-time/image_5.jpg" alt="Single Image">
                </div>
                <!--/ Image Wrapper -->

              </div>
              <!--/ Column -->
            </div>
            <!-- Wrapper -->

          </div>
          <!--/ Section -->

          <!-- Section -->
          <div class="section">

            <!-- Wrapper (Small) -->
            <div class="wrapper-small">

              <!-- Column -->
              <div class="c-col-12">

                <!-- Text Wrapper -->
                <div class="text-wrapper align-center">

                  <div class="caption">Suitable Material for Laser Cutting</div>

                  <h2 class="thin has-animation lines-up">

                    Laser cutting is suitable for plastics (acrylic, polyamide, etc.), organic materials (such as wood, paper, leather, food) and metal foils.
The following table gives an overview of the materials for laser cutting:

                  </h2>

                </div>
                <!--/ Text Wrapper -->

              </div>
              <!--/ Column -->

            </div>
            <!--/ Wrapper (Small) -->

          </div>
          <!--/ Section -->


        </div>
        <!-- Project Content -->

        <!-- Next Project Setion -->
        <div class="projects-nav">


          <div class="next-project">
            <!-- Next Project URL -->
            <a href="project-5.html">
              <div class="next-project-wrapper">

                <span class="next-project-span has-animation skew-up">>Layanan Lainnya</span>

                <!-- Next Project Title -->
                <h1 class="next-project-title has-animation skew-up">Produk Promosi Lainnya</h1>
                <!--/ Next Project Title -->
              </div>

              <!-- Next Project Image -->
              <div class="next-project-image-wrapper">
                <img src="<?php echo base_url('assets_frontend/light/images/laser-services/header1.jpg')?>" alt="Project Header Image">
              </div>
              <!--/ Next Project Image -->

            </a>
            <!--/ Next Project URL -->
          </div>


        </div>
        <!--/ Next Project Setion -->

      </div>
      <!--/ Single Project -->
