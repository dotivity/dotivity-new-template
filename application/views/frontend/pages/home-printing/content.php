<!-- Sıte Content -->
<div id="main" class="content">

  <!-- Page Settings -->
  <div class="page-settings" data-layout="dark" data-header-style="light" data-menu-layout="light"></div>
  <!--/ Page settings -->

  <!-- Portfolio Showcase -->
  <div class="fullscreen portfolio-showcase wall">

    <!--/ Wall Wrapper -->
    <div class="wall-wrapper">

      <!-- Project -->
      <div class="wall-project">

        <!-- Project URL --><a href="project-1.html">

        <!-- Project Image -->
        <div class="project-image">
          <img src="../light/images/projects/deals-app/wall_cov.jpg" alt="Wall Project Image">
        </div>
        <!--/ Project Image -->

        <!-- Project Title -->
        <div class="title">
          Cutting Sticker
        </div>
        <!--/ Project Title -->

        <!-- Project Category -->
        <div class="category">
          Detail
        </div>
        <!--/ Project Category -->
      </a>
    </div>
    <!--/ Project -->

    <!-- Project -->
    <div class="wall-project">

      <!-- Project URL --><a href="project-4.html">

      <!-- Project Image -->
      <div class="project-image">
        <video autoplay muted loop playsinline>
          <source src="http://agcotomotiv.com.tr/wall_cov-2.mp4">
          </video>
        </div>
        <!--/ Project Image -->

        <!-- Project Title -->
        <div class="title">
          Laser Services
        </div>
        <!--/ Project Title -->

        <!-- Project Category -->
        <div class="category">
          Detail
        </div>
        <!--/ Project Category -->
      </a>
    </div>
    <!--/ Project -->

    <!-- Project -->
    <div class="wall-project">

      <!-- Project URL --><a href="project-2.html">

      <!-- Project Image -->
      <div class="project-image">
        <img src="../light/images/projects/havet/wall_cov.jpg" alt="Wall Project Image">
      </div>
      <!--/ Project Image -->

      <!-- Project Title -->
      <div class="title">
        Home Printing
      </div>
      <!--/ Project Title -->

      <!-- Project Category -->
      <div class="category">
        Detail
      </div>
      <!--/ Project Category -->
    </a>
  </div>
  <!--/ Project -->


  <!-- Project -->
  <div class="wall-project">

    <!-- Project URL --><a href="project-8.html">

    <!-- Project Image -->
    <div class="project-image">
      <img src="../light/images/projects/ezra-brooks/wall_cov.jpg" alt="Wall Project Image">
    </div>
    <!--/ Project Image -->

    <!-- Project Title -->
    <div class="title">
      Advertising Product
    </div>
    <!--/ Project Title -->

    <!-- Project Category -->
    <div class="category">
      Detail
    </div>
    <!--/ Project Category -->
  </a>
</div>
<!--/ Project -->


<!-- Project -->
<div class="wall-project">

  <!-- Project URL --><a href="project-6.html">

  <!-- Project Image -->
  <div class="project-image">
    <img src="../light/images/projects/meskaline-studio/wall_cov.jpg" alt="Wall Project Image">
  </div>
  <!--/ Project Image -->

  <!-- Project Title -->
  <div class="title">
    Our Projects
  </div>
  <!--/ Project Title -->

  <!-- Project Category -->
  <div class="category">
    Detail
  </div>
  <!--/ Project Category -->
</a>
</div>
<!--/ Project -->

<!-- Project -->
<div class="wall-project">

  <!-- Project URL --><a href="project-7.html">

  <!-- Project Image -->
  <div class="project-image">
    <img src="../light/images/projects/codicis/wall_cov.jpg" alt="Wall Project Image">
  </div>
  <!--/ Project Image -->

  <!-- Project Title -->
  <div class="title">
    Our Shop
  </div>
  <!--/ Project Title -->

  <!-- Project Category -->
  <div class="category">
    Buy
  </div>
  <!--/ Project Category -->
</a>
</div>
<!--/ Project -->

</div>
</div>
<!--/ Portfolio Showcase -->
