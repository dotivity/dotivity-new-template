<!-- Sıte Content -->
<div id="main" class="content">

  <!-- Page Settings -->
  <div class="page-settings" data-layout="dark" data-header-style="light" data-menu-layout="light"></div>
  <!--/ Page settings -->

  <!-- Portfolio Showcase -->
  <div class="fullscreen portfolio-showcase wall">

    <!--/ Wall Wrapper -->
    <div class="wall-wrapper">

      <!-- Project -->
      <div class="wall-project">

        <!-- Project URL --><a href="<?php echo site_url('sticker') ?>">

        <!-- Project Image -->
        <div class="project-image">
          <img src="<?php echo base_url('assets_frontend/light/images/cutting-sticker/section-1.jpg')?>" alt="Wall Project Image">
        </div>
        <!--/ Project Image -->

        <!-- Project Title -->
        <div class="title">
          Stiker Kustom
        </div>
        <!--/ Project Title -->

        <!-- Project Category -->
        <div class="category">
          Detail
        </div>
        <!--/ Project Category -->
      </a>
    </div>
    <!--/ Project -->

    <!-- Project -->
    <div class="wall-project">

      <!-- Project URL --><a href="<?php echo site_url('laser')?>">

      <!-- Project Image -->
      <div class="project-image">
        <img src="<?php echo base_url('assets_frontend/light/images/laser-services/header1.jpg')?>" alt="Wall Project Image">
      </div>
        <!--/ Project Image -->

        <!-- Project Title -->
        <div class="title">
          Jasa Laser Cutting
        </div>
        <!--/ Project Title -->

        <!-- Project Category -->
        <div class="category">
          Detail
        </div>
        <!--/ Project Category -->
      </a>
    </div>
    <!--/ Project -->

    <!-- Project -->
    <div class="wall-project">

      <!-- Project URL --><a href="<?php echo site_url('printing')?>">

      <!-- Project Image -->
      <div class="project-image">
        <img src="<?php echo base_url('assets_frontend/light/images/home-printing/header1.jpg')?>" alt="Wall Project Image">
      </div>
      <!--/ Project Image -->

      <!-- Project Title -->
      <div class="title">
        Cetak Amplop
      </div>
      <!--/ Project Title -->

      <!-- Project Category -->
      <div class="category">
        Detail
      </div>
      <!--/ Project Category -->
    </a>
  </div>
  <!--/ Project -->


  <!-- Project -->
  <div class="wall-project">

    <!-- Project URL --><a href="<?php echo site_url('advertising')?>">

    <!-- Project Image -->
    <div class="project-image">
      <img src="../light/images/projects/ezra-brooks/wall_cov.jpg" alt="Wall Project Image">
    </div>
    <!--/ Project Image -->

    <!-- Project Title -->
    <div class="title">
      Produk Promosi Lainnya
    </div>
    <!--/ Project Title -->

    <!-- Project Category -->
    <div class="category">
      Detail
    </div>
    <!--/ Project Category -->
  </a>
</div>
<!--/ Project -->


<!-- Project -->
<div class="wall-project">

  <!-- Project URL --><a href="<?php echo site_url('projects')?>">

  <!-- Project Image -->
  <div class="project-image">
    <img src="../light/images/projects/meskaline-studio/wall_cov.jpg" alt="Wall Project Image">
  </div>
  <!--/ Project Image -->

  <!-- Project Title -->
  <div class="title">
    Proyek Kami
  </div>
  <!--/ Project Title -->

  <!-- Project Category -->
  <div class="category">
    Detail
  </div>
  <!--/ Project Category -->
</a>
</div>
<!--/ Project -->

<!-- Project -->
<div class="wall-project">

  <!-- Project URL --><a href="<?php echo site_url('shop')?>">

  <!-- Project Image -->
  <div class="project-image">
    <img src="../light/images/projects/codicis/wall_cov.jpg" alt="Wall Project Image">
  </div>
  <!--/ Project Image -->

  <!-- Project Title -->
  <div class="title" style="color: #fcc21d;">
    Toko Online
  </div>
  <!--/ Project Title -->

  <!-- Project Category -->
  <div class="category">
    Buy
  </div>
  <!--/ Project Category -->
</a>
</div>
<!--/ Project -->



</div>
</div>
<!--/ Portfolio Showcase -->
