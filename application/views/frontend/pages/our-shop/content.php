<!-- Sıte Content -->
<div id="main" class="content">

      <!-- Page Settings -->
    <div class="page-settings" data-layout="dark" data-header-style="light" data-menu-layout="light"></div>
    <!--/ Page settings -->

    <!-- Portfolio Showcase -->
    <div class="fullscreen portfolio-showcase cygni-horizontal">

        <!-- Horizontal Projects -->
        <div class="cygni-horizontal-titles">
            <div class="horizontal-wrapper">

                <!-- Project -->
                <div class="horizontal-item">

                    <!-- Project Image -->
                    <div class="horizontal-image">
                        <img src="<?php echo base_url ('assets_frontend/light/images/shop/tokopedia.jpg')?>" alt="Tokopedia">
                    </div>
                    <!--/ Project Image -->

                    <!-- Project Title -->
                    <div class="title">Tokopedia</div>
                    <!--/ Project Title -->

                    <!-- Project Year -->
                    <div class="year">ayo belanja !</div>
                    <!--/ Project Year -->

                    <!--/ Project URL -->
                    <a class="project-url" href="https://www.tokopedia.com/dotivity"></a>
                    <!--/ Project URL -->

                </div>
                <!--/ Project -->

                <!-- Project -->
                <div class="horizontal-item">

                    <!-- Project Image -->
                    <div class="horizontal-image">
                        <img src="<?php echo base_url ('assets_frontend/light/images/shop/shopee.jpg')?>" alt="Shopee">
                    </div>
                    <!--/ Project Image -->

                    <!-- Project Title -->
                    <div class="title">Shopee</div>
                    <!--/ Project Title -->

                    <!-- Project Year -->
                    <div class="year">ayo belanja !</div>
                    <!--/ Project Year -->

                    <!--/ Project URL -->
                    <a class="project-url" href="https://shopee.co.id/dotivity"></a>
                    <!--/ Project URL -->

                </div>
                <!--/ Project -->

                <!-- Project -->
                <div class="horizontal-item">

                    <!-- Project Image -->
                    <div class="horizontal-image">
                        <img src="<?php echo base_url ('assets_frontend/light/images/shop/bukalapak.jpg')?>" alt="Bukalapak">
                    </div>
                    <!--/ Project Image -->

                    <!-- Project Title -->
                    <div class="title">Bukalapak</div>
                    <!--/ Project Title -->

                    <!-- Project Year -->
                    <div class="year">ayo belanja !</div>
                    <!--/ Project Year -->

                    <!--/ Project URL -->
                    <a class="project-url" href="https://www.bukalapak.com/u/dotivity"></a>
                    <!--/ Project URL -->

                </div>
                <!--/ Project -->

                <!-- Project -->
                <div class="horizontal-item">

                    <!-- Project Image -->
                    <div class="horizontal-image">
                        <img src="<?php echo base_url ('assets_frontend/light/images/shop/bukalapak.jpg')?>" alt="Lazada">
                    </div>
                    <!--/ Project Image -->

                    <!-- Project Title -->
                    <div class="title">Lazada</div>
                    <!--/ Project Title -->

                    <!-- Project Year -->
                    <div class="year">ayo belanja !</div>
                    <!--/ Project Year -->

                    <!--/ Project URL -->
                    <a class="project-url" href="https://www.lazada.co.id/shop/dotivity"></a>
                    <!--/ Project URL -->

                </div>
                <!--/ Project -->

                <!-- Project -->
                <div class="horizontal-item">

                    <!-- Project Image -->
                    <div class="horizontal-image">
                        <img src="<?php echo base_url ('assets_frontend/light/images/shop/bukalapak.jpg')?>" alt="Tiktok Shop">
                    </div>
                    <!--/ Project Image -->

                    <!-- Project Title -->
                    <div class="title">Tiktok Shop</div>
                    <!--/ Project Title -->

                    <!-- Project Year -->
                    <div class="year">ayo belanja !</div>
                    <!--/ Project Year -->

                    <!--/ Project URL -->
                    <a class="project-url" href="https://www.tiktok.com/@dotivity"></a>
                    <!--/ Project URL -->

                </div>
                <!--/ Project -->

            </div>
        </div>
        <!--/ Horizontal Projects -->

        <!-- Discover Project Button -->
        <div class="horizontal-project-link">
            <a href="#" target="_blank">Masuk ke Toko</a>
        </div>
        <!--/ Discover Project Button -->

        <!-- Showcase Elements -->
        <div class="cygni-horizontal-images"></div>
        <div class="horizontal-pagination">
            <div class="horizontal-prev"><i class="icon-left-dir"></i></div>
            <div class="horizontal-next"><i class="icon-right-dir"></i></div>
        </div>
        <div class="horizontal-fraction">
            <div class="horizontal-progress"></div>

        </div>
        <!--/ Showcase Elements -->

    </div>
    <!--/ Portfolio Showcase -->
