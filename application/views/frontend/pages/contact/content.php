<!-- Site Content -->
<div id="main" class="content">

  <!-- Page Settings -->
  <div class="page-settings" data-layout="dark" data-header-style="light" data-menu-layout="light"></div>
  <!--/ Page settings -->

  <!-- Page Content -->
  <div id="page-content" class="page-content">

    <!-- Section -->
    <div class="section">

      <!-- Wrapper -->
      <div class="wrapper-small">

        <!-- Column -->
        <div class="c-col-12">

          <!-- Text Wrapper -->
          <div class="text-wrapper">
            <div class="caption has-animation skew-up">CONTACT US</div>
            <h1 class="big-title has-animation skew-up">Connect<br>with us <br> today.</h1>
          </div>
          <!-- Text Wrapper -->

        </div>
        <!-- Column -->

        <!-- Column -->
        <div class="c-col-6">

          <!-- Text Wrapper -->
          <div class="text-wrapper">
            <p data-delay="0.5" class="big-p has-animation lines-fade-up">
              Don't worry,<br>we're here to help you do what you want.<br> Text us, we'll text you soon.

            </p>
          </div>
          <!--/ Text Wrapper -->


        </div>
        <!--/ Column -->

        <!-- Column -->
        <div class="c-col-6">

          <!-- Text Wrapper -->
          <div class="text-wrapper">
            <div data-delay="0.7" class="caption has-animation skew-up">E-MAIL</div>
            <p data-delay="0.9" class="big-p has-animation skew-up"><a class="underline" href="mailto:hello@dotivity.id">hello@dotivity.id</a></p>
          </div>
          <!--/ Text Wrapper -->

          <span class="pe-empty-space" style="height: 60px"></span>

          <!-- Text Wrapper -->
          <div class="text-wrapper">
            <div data-delay="0.7" class="caption has-animation skew-up">ADDRESS</div>
            <p data-delay="0.9" class="big-p has-animation skew-up"><a class="underline" href="https://www.google.com/maps/place/Dotivity/@-6.2601208,106.9071406,17z/data=!3m1!4b1!4m5!3m4!1s0x2e69f3693dac1d03:0x5b9030e8eb546ee9!8m2!3d-6.2601196!4d106.9093293">
              16, Keahlian 06/05
              <br>Jaticempaka, Pondok Gede</a>
            </p>

          </div>
          <!--/ Text Wrapper -->

          <span class="pe-empty-space" style="height: 60px"></span>

          <!-- Text Wrapper -->
          <div class="text-wrapper">
            <div class="caption has-animation skew-up">WHATSAPP</div>
            <p data-delay="0.2" class="big-p has-animation skew-up"> <a class="underline" href="https://wa.me/6281388813751">+62 813 8881 3751</a></p>
          </div>
          <!--/ Text Wrapper -->

        </div>
        <!--/ Column -->


      </div>
      <!--/ Wrapper -->

    </div>
    <!--/Section -->

    <!--Section -->
    <div class="section">

      <!-- Wrapper -->
      <div class="wrapper-small">

        <!-- Column -->
        <div class="c-col-12 align-center">
          <div class="caption has-animation skew-up">SEE SOMETHING MORE</div>
          <h1 data-delay="0.2" class="big-title has-animation skew-up"><a href="<?php echo site_url('about')?>" class="underline">About Us</a></h1>
        </div>
        <!--/ Column -->

      </div>
      <!--/ Wrapper -->

    </div>
    <!--/ Section -->

  </div>
  <!-- Page Content -->

  <!-- Footer -->
  <footer class="site-footer">

    <!-- Wrapper -->
    <div style="margin-bottom: 0" class="wrapper">

      <!-- Column -->
      <div class="c-col-6">

        <!-- Footer Menu -->
        <ul class="footer-menu ">
          <li><a href="#">Terms</a></li>
          <li><a href="#">Privacy Policy</a></li>
          <li><a href="#">Purchase</a></li>
        </ul>
        <!--/ Footer Menu -->

      </div>
      <!-- Column -->

      <!-- Column -->
      <div class="c-col-6 align-right">
        <p class="copyright-text">Dotivity, © 2022</p>
      </div>
      <!-- Column -->

    </div>
    <!-- Wrapper -->


  </footer>
  <!--/ Footer -->

</div>
<!--/ Site Content -->
