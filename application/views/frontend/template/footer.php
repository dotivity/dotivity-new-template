<!-- Fullscreen Footer -->
<div class="fullscreen-footer">

    <!-- Fullscreen Footer Left Side -->
    <div class="ff-left">
        <a href="https://wa.me/6281388813751">SAY HELLO!</a>
    </div>
    <!--/ Fullscreen Footer Left Side-->

    <!-- Fullscreen Footer Right Side -->
    <div class="ff-right">
        <ul class="ff-socials">
            <li><a href="https://www.facebook.com/dotivity">Fb</a></li>
            <li><a href="https://www.instagram.com/dotivity.id/">Ig</a></li>
        </ul>
    </div>
    <!--/ Fullscreen Footer Right Side -->

</div>
<!--/ Fullscreen Footer -->

</div>
<!--/ Site Content -->
