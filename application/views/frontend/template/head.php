<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">

  <!-- Page Title -->
  <title><?php echo $title; ?></title>
  <!--/ Page Title -->

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="title" content="Industri Kreatif Online: Cutting Sticker, Laser Cutting Engraving, Home Printing, Advertising Product">
  <meta name="description" content="Temukan layanan berkualitas dari Industri Kreatif kami, termasuk pembuatan cutting sticker, laser cutting engraving & marking, home printing, dan advertising product. Dengan teknologi terkini dan kreativitas tinggi, kami siap membantu mewujudkan ide-ide kreatif Anda. Hubungi kami sekarang untuk solusi kreatif yang unik dan berkualitas!">
  <meta name="keywords" content="industri kreatif, cutting sticker, laser cutting, laser engraving, laser marking, home printing, advertising product">
  <meta name="author" content="Dotivity">
  <meta name="identifier-URL" content="https://www.dotivity.id">
  <meta property="og:title" content="Industri Kreatif Online: Cutting Sticker, Laser Cutting Engraving, Home Printing, Advertising Product">
  <meta property="og:url" content="https://www.dotivity.id"/>
  <meta property="og:type" content="website">
  <meta property="og:description" content="Temukan layanan berkualitas dari Industri Kreatif kami, termasuk pembuatan cutting sticker, laser cutting engraving & marking, home printing, dan advertising product. Dengan teknologi terkini dan kreativitas tinggi, kami siap membantu mewujudkan ide-ide kreatif Anda. Hubungi kami sekarang untuk solusi kreatif yang unik dan berkualitas!">
  <meta property="og:image" content="<?php echo base_url ('assets_frontend/meta/meta-home.jpg')?>">
  <meta property="og:site_name" content="Industri Kreatif Online - Dotivity"/>

  <link href="https://fonts.googleapis.com/css?family=Archivo:400,500,600,700&amp;display=swap" rel="stylesheet">

  <link href="<?php echo base_url ('assets_frontend/light/css/plugins.css')?>" rel="stylesheet">
  <link href="<?php echo base_url ('assets_frontend/light/css/entypo.css')?>" rel="stylesheet">

  <link href="<?php echo base_url ('assets_frontend/light/style.css')?>" rel="stylesheet">

  <!-- Favicons -->
  <link rel="shortcut icon" href="<?php echo base_url ('assets_frontend/light/images/favicon.png')?>" />
  <link rel="apple-touch-icon" href="<?php echo base_url ('assets_frontend/light/images/favicon.png')?>" />
  <!--/ Favicons -->

  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135966916-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-135966916-1');
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-GHNFJNDY3W"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-GHNFJNDY3W');
</script>

</head>
