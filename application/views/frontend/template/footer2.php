<!-- Footer -->
<footer class="site-footer">


  <!-- Fullwidth Wrapper -->
  <div class="wrapper">

    <!-- Column -->
    <div class="c-col-12">
      <h1 class="thin" style="margin: 0">Kamu punya ide atau <em>project</em>?
        <br><a class="underline" href="mailto:hello@dotivity.id">hello@dotivity.id</a>
        <br><a class="underline" href="https://wa.me/6281388813751">0813-8881-3751</a>
      </h1>

    </div>
    <!--/ Column -->

  </div>
  <!-- Fullwidth Wrapper -->

  <!-- Fullwidth Wrapper -->
  <div style="margin-bottom: 75px" class="wrapper-full">

    <div class="c-col-6 hide-mobile"></div>

    <!-- Column -->
    <div class="c-col-3 no-gap">
      <div class="caption">VISIT</div>
      <h5>17, Keahlian 06/05
      <br>Jaticempaka, Pondok Gede<br>
    Bekasi, Jawa Barat</h5>

    </div>
    <!--/ Column -->

    <!-- Column -->
    <div class="c-col-3 no-gap">
      <div class="caption">FOLLOW</div>

      <!-- Footer List -->
      <ul class="footer-list">
        <li><a href="https://www.facebook.com/dotivity" class="underline">Facebook</a></li>
        <li><a href="https://www.instagram.com/dotivity.id" class="underline">Instagram</a></li>
      </ul>
      <!-- Footer List -->
    </div>
    <!--/ Column -->

    

  </div>
  <!-- Fullwidth Wrapper -->

  <!-- Wrapper -->
  <div style="margin-bottom: 0" class="wrapper">

    <!-- Column -->
    <div class="c-col-6">

      <!-- Footer Menu -->
      <ul class="footer-menu ">
        <li><a href="https://www.tokopedia.com/dotivity">Tokopedia</a></li>
        <li><a href="https://shopee.co.id/dotivity">Shopee</a></li>
        <li><a href="https://www.bukalapak.com/u/dotivity">Bukalapak</a></li>
        <li><a href="https://www.lazada.co.id/shop/dotivity">Lazada</a></li>
        <li><a href="https://www.tiktok.com/@dotivity">Tiktok Shop</a></li>
      </ul>
      <!--/ Footer Menu -->

    </div>
    <!-- Column -->

    <!-- Column -->
    <div class="c-col-6 align-right">
      <p class="copyright-text">Dotivity, © 2017</p>
    </div>
    <!-- Column -->

  </div>
  <!-- Wrapper -->


</footer>
<!--/ Footer -->

</div>
