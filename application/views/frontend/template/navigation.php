<!-- Header -->
<header class="site-header">

  <!-- Branding -->
  <div class="site-branding">
    <a href="<?php echo site_url('')?>"><img class="dark-logo" src="<?php echo base_url ('assets_frontend/light/images/logo.png')?>" alt="Site Logo"></a>
    <a href="<?php echo site_url('')?>"><img class="light-logo" src="<?php echo base_url ('assets_frontend/light/images/logo-dark.png')?>" alt="Site Light Logo"></a>
  </div>
  <!-- /Branding -->
  <!-- Menu -->
  <div class="site-navigation">

    <!-- Menu Toggle (Don't Touch) -->
    <div class="menu-toggle"></div>
    <!-- /Menu Toggle -->

    <!-- Menu Overlays (Don't Touch) -->
    <div class="menu-overlays"></div>
    <!-- Menu Overlays -->

    <div class="menu-wrapper">

      <!-- Navigation -->
      <ul class="menu">

        <!-- Main Menu -->
        <li class="menu-item"><a href="<?php echo site_url('about')?>">Tentang Kami</a></li>
        <li class="menu-item has-children menu-item-active"><a href="<?php echo site_url('shop')?>">Toko Online</a>
          <!-- Sub Menu -->
          <ul class="sub-menu">
            <li class="menu-item"><a href="https://www.tokopedia.com/dotivity">Tokopedia</a></li>
            <li class="menu-item"><a href="https://shopee.co.id/dotivity">Shopee</a></li>
            <li class="menu-item"><a href="https://www.bukalapak.com/u/dotivity">Bukalapak</a></li>

          </ul>
          <!-- /Navigation -->
        </li>
        <li class="menu-item"><a href="<?php echo site_url('contact')?>">Kontak</a></li>
        <!-- /Main Menu -->
        <!-- /Navigation -->

      </div>

      <!-- Menu Widget (Left) -->
      <div class="menu-widget-wrapper mww-1">
        <div class="menu-widget">
          <div class="menu-widget-title">
            KAMI SELALU ADA DISINI;
          </div>

          <a class="underline" href="https://www.google.com/maps/place/Dotivity/@-6.2601208,106.9071406,17z/data=!3m1!4b1!4m5!3m4!1s0x2e69f3693dac1d03:0x5b9030e8eb546ee9!8m2!3d-6.2601196!4d106.9093293">
            <h5>17, Keahlian 06/05
              <br>Jaticempaka, Pondok Gede</h5></a>
              <a class="underline" href="https://wa.me/6281388813751"><h5>+62 813 8881 3751</h5></a>
            </div>
          </div>
          <!--/ Menu Widget (Left) -->

          <!-- Menu Widget (Middle) -->
          <div class="menu-widget-wrapper mww-2">
            <div class="scrolling-button">
              <a href="mailto:hello@dotivity.id">hello@dotivity.id</a>
            </div>
          </div>
          <!--/ Menu Widget (Middle) -->

          <!-- Menu Widget (Right -->
          <div class="menu-widget-wrapper mww-3">
            <div class="menu-widget">
              <ul class="widget-socials">
                <li><a href="https://www.facebook.com/dotivity">facebook.</a></li>
                <li><a href="https://instagram.com/dotivity.id/">instagram.</a></li>
              </ul>
            </div>
          </div>
          <!--/ Menu Widget (Right) -->

        </div>
        <!-- /Menu -->
      </header>
      <!-- /Header -->
