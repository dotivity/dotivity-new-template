<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="author" content="Dotivity" />


  <!-- Stylesheets
  ============================================= -->
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url ('assets_portfolio/basic/css/bootstrap.css')?>" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url ('assets_portfolio/basic/style.css')?>" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url ('assets_portfolio/basic/css/dark.css')?>" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url ('assets_portfolio/basic/css/font-icons.css')?>" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url ('assets_portfolio/basic/css/animate.css')?>" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url ('assets_portfolio/basic/css/magnific-popup.css')?>" type="text/css" />

  <link rel="stylesheet" href="<?php echo base_url ('assets_portfolio/basic/css/responsive.css')?>" type="text/css" />
  <link rel="shortcut icon" href="<?php echo base_url ('assets_frontend/images/favicon.ico')?>">

  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <!-- Document Title
  ============================================= -->
  <title>404 - Dotivity</title>

</head>

<body class="stretched dark">

  <!-- Document Wrapper
  ============================================= -->
  <div id="wrapper" class="clearfix">

    <!-- Header
    ============================================= -->
    <header id="header">

      <div id="header-wrap">

        <div class="container clearfix">

          <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

          <!-- Logo
          ============================================= -->
          <div id="logo">
            <a href="<?php echo base_url()?>" class="standard-logo" data-dark-logo="<?php echo base_url ('assets_frontend/images/logo-dark.png')?>"><img src="<?php echo base_url ('assets_frontend/images/logo.png')?>" alt="Dotivity"></a>
            <a href="<?php echo base_url()?>" class="retina-logo" data-dark-logo="<?php echo base_url ('assets_frontend/images/logo-dark@2x.png')?>"><img src="<?php echo base_url ('assets_frontend/images/logo@2x.png')?>" alt="Dotivity"></a>
          </div><!-- #logo end -->



        </div>

      </div>

    </header><!-- #header end -->


    <section id="slider" class="slider-element slider-parallax full-screen dark error404-wrap" style="background: url(<?php echo base_url ('assets_frontend/assets/8aadb-Artboard-33-1111.jpg')?>) center;">
      <div class="slider-parallax-inner">

        <div class="container-fluid vertical-middle center clearfix">

          <div class="error404">404</div>

          <div class="heading-block nobottomborder">
            <h4>Ooopps.! The Page you were looking for, couldn't be found.</h4>
            <span>Try the search below to find matching pages:</span>
          </div>

        </div>

      </div>
    </section>

    <!-- Footer
    ============================================= -->
    <footer id="footer" class="dark">

      <div class="container">

        <!-- Footer Widgets
        ============================================= -->
        <div class="footer-widgets-wrap clearfix">

          <div class="col_one_third">

            <div class="widget clearfix">

              <img src="<?php echo base_url ('assets_frontend/assets/d0454-Artboard-45-111.png')?>" alt="" class="footer-logo" style="width:75%">

              <p>Dotivity adalah sebuah jasa yang bergerak di bidang industri kreatif yang secara khusus berfokus pada bidang Desain Logo, Website, Cutting Laser Engraving Marking, &amp; Cutting Sticker.</p>

              <div style="background: url('<?php echo base_url ('assets_portfolio/basic/images/world-map.png')?>') no-repeat center center; background-size: 100%;">
                <address>
                  <strong>LOCATION</strong><br>
                  Jaticempaka, Pondok Gede<br>
                  Bekasi, 17411<br>
                </address>
                <abbr title="Phone Number"><strong>Phone:</strong></abbr> (+62) 813 8881 3751<br>
                <abbr title="Email Address"><strong>Email:</strong></abbr> hello@dotivity.id
              </div>

            </div>

          </div>

          <div class="col_one_third">

            <div class="widget clearfix">
              <h4>Sosial Media</h4>
            </div>

            <div class="widget clearfix">

              <a href="https://www.facebook.com/dotivity" class="social-icon si-small si-rounded si-facebook" target="_blank">
                <i class="icon-facebook"></i>
                <i class="icon-facebook"></i>
              </a>

              <a href="https://www.instagram.com/dotivity.id" class="social-icon si-small si-rounded si-instagram" target="_blank">
                <i class="icon-instagram"></i>
                <i class="icon-instagram"></i>
              </a>

              <a href="https://api.whatsapp.com/send?phone=6281905156020&text=Hello!" class="social-icon si-small si-rounded si-whatsapp" target="_blank">
                <i class="icon-whatsapp"></i>
                <i class="icon-whatsapp"></i>
              </a>


            </div>

          </div>

          <div class="col_one_third col_last">

            <div id="instagram" class="widget clearfix">

              <h4 class="highlight-me">Instagram </h4>
              <div id="instagram-photos" class="instagram-photos masonry-thumbs grid-4" data-user="4021452242" data-count="12" data-type="user"></div>

            </div>

          </div>

        </div><!-- .footer-widgets-wrap end -->

      </div>

      <!-- Copyrights
      ============================================= -->
      <div id="copyrights">

        <div class="container clearfix">

          <div class="col_half">
            Copyrights &copy; 2020 All Rights Reserved by <a href="<?php echo base_url()?>">Dotivity</a>.
          </div>


        </div>

      </div><!-- #copyrights end -->

    </footer><!-- #footer end -->


  </div><!-- #wrapper end -->

  <!-- Go To Top
  ============================================= -->
  <div id="gotoTop" class="icon-angle-up"></div>

  <!-- External JavaScripts
  ============================================= -->
  <script src="<?php echo base_url ('assets_portfolio/basic/js/jquery.js')?>"></script>
  <script src="<?php echo base_url ('assets_portfolio/basic/js/plugins.js')?>"></script>

  <!-- Footer Scripts
  ============================================= -->
  <script src="<?php echo base_url ('assets_portfolio/basic/js/functions.js')?>"></script>

</body>
</html>
