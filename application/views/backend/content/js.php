<!-- BEGIN: Vendor JS-->
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/vendors.min.js')?>"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/tables/ag-grid/ag-grid-community.min.noStyle.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/forms/select/select2.full.min.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/pickers/pickadate/picker.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/pickers/pickadate/picker.date.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/extensions/jquery.steps.min.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/forms/validation/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/extensions/dropzone.min.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/tables/datatable/datatables.min.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js')?>"></script>
<!--<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/tables/datatable/dataTables.select.min.js')?>"></script>-->
<script src="<?php echo base_url ('assets_backend/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js')?>"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="<?php echo base_url ('assets_backend/app-assets/js/core/app-menu.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/js/core/app.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/js/scripts/components.js')?>"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="<?php echo base_url ('assets_backend/app-assets/js/scripts/pages/app-user.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/js/scripts/navs/navs.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/js/scripts/forms/wizard-steps.js')?>"></script>
<script src="<?php echo base_url ('assets_backend/app-assets/js/scripts/ui/data-list-view.js')?>"></script>

<script src="<?php echo base_url ('assets_backend/assets/js/ckeditor/ckeditor.js')?>"></script>


<!-- END: Page JS-->
<script>
$(function () {
  // Replace the <textarea id="editor1"> with a CKEditor
  // instance, using default configuration.
  CKEDITOR.replace('editor')
  //bootstrap WYSIHTML5 - text editor
  $('.textarea').wysihtml5()
})
</script>

<script>
$(function () {
  // Replace the <textarea id="editor1"> with a CKEditor
  // instance, using default configuration.
  CKEDITOR.replace('editor1')
  //bootstrap WYSIHTML5 - text editor
  $('.textarea').wysihtml5()
})
</script>
