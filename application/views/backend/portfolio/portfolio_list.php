

<?php
include 'html.php';
include 'css.php'; ?>

</head>

<?php include 'header.php'; ?>

<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
  <div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
      <li class="nav-item mr-auto"><a class="navbar-brand" href="<?php echo site_url ('backend/dashboard')?>">
        <div class="brand-logo"></div>
        <h2 class="brand-text mb-0">BACKEND</h2>
      </a></li>
      <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="icon-disc"></i></a></li>
    </ul>
  </div>
  <div class="shadow-bottom"></div>
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      <li class=" nav-item"><a href="#"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>
        <ul class="menu-content">
          <li><a href="<?=site_url('backend/dashboard')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">Home</span></a>
          </li>
        </ul>
      </li>

      <?php if ($this->session->userdata('admin_area') != 0): ?>
        <li class=" navigation-header"><span>ACCOUNT</span>
        </li>
        <li class=" nav-item"><a href="#"><i class="feather icon-user"></i><span class="menu-title" data-i18n="User">User</span></a>
          <ul class="menu-content">
            <li><a href="<?php echo site_url('backend/user/user_detail/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">My Account</span></a>
            </li>
            <li><a href="<?php echo site_url('backend/user/user_view/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List">List Account</span></a>
            </li>
            <li><a href="<?php echo site_url('backend/user/user_add/'.$this->session->userdata('cibb_user_id')); ?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="List">Add Account</span></a>
            </li>
          </ul>
        </li>
      <?php endif; ?>
      <li class=" navigation-header"><span>WEBSITE</span>
      </li>
      <li class=" nav-item"><a href="#"><i class="feather icon-briefcase"></i><span class="menu-title" data-i18n="User">Content</span></a>
        <ul class="menu-content">
          <li  class="active"><a href="<?php echo site_url('backend/portfolio')?>"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="View">Portfolio</span></a>
          </li>

        </ul>
      </li>

    </ul>
  </div>
</div>

<!--content -->
<div class="app-content content">
  <div class="content-overlay"></div>
  <div class="header-navbar-shadow"></div>
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
          <div class="col-12">
          </div>
        </div>
      </div>
      <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
      </div>
    </div>
    <div class="content-body">
      <!-- Data list view starts -->
      <section id="data-thumb-view" class="data-thumb-view-header">
        <div class="action-btns d-none">
          <div class="btn-dropdown mr-1 mb-1">
          </div>
        </div>
        <!-- dataTable starts -->
        <div class="table-responsive">
          <table class="table data-thumb-view">
            <thead>
              <tr>
                <th></th>
                <th>IMAGE</th>
                <th>TITLE</th>
                <th>CATEGORY</th>
                <th>STATUS</th>
                <th>DATE</th>
                <th>ACTION</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($data as $data): ?>
                <tr>
                  <td id="<?php echo $data->pic_id?>"></td>
                  <td class="product-img"><img src="<?=base_url()?>uploads/files/<?=$data->pic_file?>" alt="<?php echo $data->pic_title?>">
                  </td>
                  <td class="product-name"><?php echo $data->pic_title?></td>
                  <td class="product-category"><?php echo $data->pic_category?></td>
                  <td>
                    <div class="chip chip-success">
                      <div class="chip-body">
                        <div class="chip-text">UPLOADED</div>
                      </div>
                    </div>
                  </td>
                  <td class="product-price"><?php echo $data->date?></td>
                  <td class="product-action">
                    <a hidden class="action-edit" href="<?=site_url()?>backend/Portfolio/edit/<?=$data->pic_id?>"><i class="feather icon-edit"></i></a>
                    <div class="modal-danger mr-1 mb-1 d-inline-block">
                      <!-- Button trigger modal -->
                      <button type="button" class="btn bg-gradient-danger mr-1 mb-1 waves-effect waves-light" data-toggle="modal" data-target="#danger<?=$data->pic_id?>">
                        Delete
                      </a>
                    </div>
                  </td>
                </tr>

                <div class="modal fade text-left" id="danger<?=$data->pic_id?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel120" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                      <div class="modal-header bg-danger white">
                        <h5 class="modal-title">Delete <?php echo $data->pic_title?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        Are you sure want to delete this data ?
                      </div>
                      <div class="modal-footer">
                        <input type="hidden" name="pic_id" value="<?=$data->pic_id?>">
                        <input type="hidden" name="pic_file" value="<?=$data->pic_file?>">
                        <input type="hidden" name="pic_file1" value="<?=$data->pic_file1?>">
                        <input type="hidden" name="pic_file2" value="<?=$data->pic_file2?>">
                        <input type="hidden" name="pic_file3" value="<?=$data->pic_file3?>">
                        <input type="hidden" name="pic_file4" value="<?=$data->pic_file4?>">
                        <a class="btn btn-danger action-delete" data-dismiss="modal" href="<?=site_url()?>backend/Portfolio/deletedata/<?=$data->pic_id?>/<?=$data->pic_file?>/<?=$data->pic_file1?>/<?=$data->pic_file2?>/<?=$data->pic_file3?>/<?=$data->pic_file4?>">Yes, delete please</a>
                    </div>
                  </div>
                </div>
              </div>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
      <!-- dataTable ends -->

      <!-- add new sidebar starts -->

      <div class="add-new-data-sidebar">
        <div class="overlay-bg"></div>
        <?php echo validation_errors(); ?>
        <?php if(isset($error)){print $error;}?>
        <?php echo form_open_multipart('backend/portfolio/insertdata');?>
        <div class="add-new-data">
          <div class="div mt-2 px-2 d-flex new-data-title justify-content-between">
            <div>
              <h4 class="text-uppercase">Upload Portfolio</h4>
            </div>
            <div class="hide-data-sidebar">
              <i class="feather icon-x"></i>
            </div>
          </div>
          <form action="" method="post" class="dropzone">
            <div class="data-items pb-3">
              <div class="data-fields px-2 mt-3">
                <div class="row">

                  <div class="col-sm-12 data-field-col">
                    <label for="pic_title">Title</label>
                    <input type="text"  name="pic_title" class="form-control" id="pic_title" value="<?= set_value('pic_title'); ?>">
                  </div>
                  <div class="col-sm-12 data-field-col">
                    <label for="pic_category"> Category </label>
                    <select class="form-control" id="pic_category" name="pic_category" value="<?= set_value('pic_category'); ?>">
                      <option value="Logo">Logo</option>
                      <option value="Website">Website</option>
                      <option value="Laser">Laser</option>
                      <option value="Sticker">Sticker</option>
                    </select>
                  </div>
                  <div class="col-sm-12 data-field-col">
                    <fieldset class="form-label-group">
                      <textarea name="pic_desc" class="form-control" id="label-textarea" rows="3" placeholder="Description"></textarea>
                      <label for="label-textarea">Description</label>
                    </fieldset>
                  </div>
                  <div class="col-sm-12 data-field-col">
                    <fieldset class="form-label-group">
                      <form>
                        <input type='text' class="form-control pickadate" name="pic_date_picker"/>
                      </form>
                      <label for="label-textarea">Date Picker</label>
                    </fieldset>
                  </div>
                  <div class="col-sm-12 data-field-col data-list-upload">
                    <input type="file" class="dropzone dropzone-area" name="userfile[]" multiple required>
                    <div class="dz-message">Upload Image</div>
                  </input>
                </div>

              </div>
            </div>
          </div>
        </form>

        <div class="add-data-footer d-flex justify-content-around px-3 mt-2">
          <div class="add-data-btn">
            <button class="btn btn-primary" type="submit" name="fileSubmit">Upload</button>
          </div>
          <div class="cancel-data-btn">
            <button class="btn btn-outline-danger">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    <!-- add new sidebar ends -->
  </section>
  <!-- Data list view end -->

</div>
</div>
</div>

<?php
include 'footer.php';
include 'js.php';
include 'end.php';?>
